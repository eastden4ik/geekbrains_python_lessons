"""
    Реализовать класс Road (дорога), в котором определить атрибуты: length (длина), width (ширина).
    Значения данных атрибутов должны передаваться при создании экземпляра класса.
    Атрибуты сделать защищенными. Определить метод расчета массы асфальта,
    необходимого для покрытия всего дорожного полотна. Использовать формулу:
    длина*ширина*масса асфальта для покрытия одного кв метра дороги асфальтом, толщиной в 1 см*число см толщины полотна.
    Проверить работу метода.

    Например: 20м*5000м*25кг*5см = 12500 т
"""


class Road:

    _width = None
    _length = None

    def __init__(self, length: float, width: float):
        self._length = length
        self._width = width

    def get_mas(self, asphalt_square: float, depth: float) -> float:
        mas_of_asphalt = self._length * self._width * asphalt_square * depth / 1000
        return mas_of_asphalt
        

if __name__ == '__main__':
    road = Road(20, 5000)
    mas = road.get_mas(asphalt_square=25, depth=5)
    print(f'{mas} т')
