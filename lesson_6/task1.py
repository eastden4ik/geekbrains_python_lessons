"""
    Создать класс TrafficLight (светофор) и определить у него один атрибут color (цвет) и метод running (запуск).
    Атрибут реализовать как приватный. В рамках метода реализовать переключение светофора в режимы:
    красный, желтый, зеленый.
    Продолжительность первого состояния (красный) составляет 7 секунд, второго (желтый) — 2 секунды,
    третьего (зеленый) — на ваше усмотрение.
    Переключение между режимами должно осуществляться только в указанном порядке (красный, желтый, зеленый).
    Проверить работу примера, создав экземпляр и вызвав описанный метод.
"""
from time import sleep


class TrafficLight:

    __color: str
    __modes = {
        'красный': 7,
        'желтый': 2,
        'зеленый': 10
    }

    def running(self):
        for color, wait_sec in self.__modes.items():
            self.__color = color
            print(f'Wait color {self.__color} for {wait_sec} seconds.')
            sleep(wait_sec)


if __name__ == '__main__':
    trafficLight = TrafficLight()
    trafficLight.running()
