"""
    Реализовать класс Stationery (канцелярская принадлежность).
    Определить в нем атрибут title (название) и метод draw (отрисовка).
    Метод выводит сообщение “Запуск отрисовки.”
    Создать три дочерних класса Pen (ручка), Pencil (карандаш), Handle (маркер).
    В каждом из классов реализовать переопределение метода draw.
    Для каждого из классов метод должен выводить уникальное сообщение.
    Создать экземпляры классов и проверить, что выведет описанный метод для каждого экземпляра.
"""


class Stationery:
    _title = ''
    _message = 'Запуск отрисовки'

    def draw(self):
        print(self._title, '->', self._message)


class Pen(Stationery):
    _title = 'Pen'
    _message = 'Запуск отрисовки ручкой'


class Pencil(Stationery):
    _title = 'Pencil'
    _message = 'Запуск отрисовки карандашом'


class Handle(Stationery):
    _title = 'Handle'
    _message = 'ЗЗапуск отрисовки маркером'


if __name__ == '__main__':
    pen = Pen()
    pencil = Pencil()
    handle = Handle()

    pen.draw()
    pencil.draw()
    handle.draw()
