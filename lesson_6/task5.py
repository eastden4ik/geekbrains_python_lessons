"""
    Создайте экземпляры классов, передайте значения атрибутов.
    Выполните доступ к атрибутам, выведите результат.
    Выполните вызов методов и также покажите результат.
"""
from lesson_6.task4 import TownCar, SportCar, WorkCar, PoliceCar

if __name__ == '__main__':
    town_car = TownCar(name="Ford Focus", color="Blue", speed=58)
    sport_car = SportCar(name="Ferrari F40", color="Red", speed=284)
    work_car = WorkCar(name="Chevrolet Silverado", color="White", speed=45)
    police_car = PoliceCar(name="Dodge Charger", color="Black & White", speed=92)

    print(f"{town_car.color} {town_car.go()} со скоростью {town_car.show_speed()}")
    print(f"{sport_car.color} {sport_car.go()} со скоростью {sport_car.show_speed()}")
    print(
        f"{police_car.name} не остановила {sport_car.name}, т.к. не смогла разогнаться выше {police_car.show_speed()}")
    print(f"{police_car.name} остановила {work_car.name}, двигавшуюся со скоростью {work_car.show_speed()}")