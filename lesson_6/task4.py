"""
    Реализуйте базовый класс Car.
    У данного класса должны быть следующие атрибуты: speed, color, name, is_police (булево).
    А также методы: go, stop, turn(direction), которые должны сообщать, что машина поехала, остановилась,
    повернула (куда).

    Опишите несколько дочерних классов: TownCar, SportCar, WorkCar, PoliceCar.

    Добавьте в базовый класс метод show_speed, который должен показывать текущую скорость автомобиля.
    Для классов TownCar и WorkCar переопределите метод show_speed.
    При значении скорости свыше 60 (TownCar) и 40 (WorkCar) должно выводиться сообщение о превышении скорости.
"""


class Car:

    def __init__(self, speed: float, color: str, name: str):
        self.speed = speed
        self.color = color
        self.name = name
        self.is_police = False

    def go(self) -> str:
        return f'машина {self.name} поехала вперед'

    def stop(self) -> str:
        return f'машина {self.name} остановилась'

    def turn(self, direction: str) -> str:
        return f'машина {self.name} повернула на {direction}'

    def show_speed(self) -> str:
        return f'{self.speed} км/ч'


class TownCar(Car):

    __max_speed = 60

    def show_speed(self) -> str:
        if self.speed > self.__max_speed:
            return f'{self.speed - self.__max_speed} км/ч'
        return f'{self.speed} км/ч'


class SportCar(Car):
    pass


class WorkCar(Car):

    __max_speed = 40

    def show_speed(self) -> str:
        if self.speed > self.__max_speed:
            return f'{self.speed - self.__max_speed} км/ч'
        return f'{self.speed} км/ч'


class PoliceCar(Car):
    is_police: bool = True
