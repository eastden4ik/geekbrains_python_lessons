"""
    Реализовать базовый класс Worker (работник),
    в котором определить атрибуты:
    name, surname, position (должность), income (доход).

    Последний атрибут должен быть защищенным и ссылаться на словарь, содержащий элементы:
    оклад и премия, например, {"wage": wage, "bonus": bonus}.

    Создать класс Position (должность) на базе класса Worker.

    В классе Position реализовать методы получения
    полного имени сотрудника (get_full_name) и дохода с учетом премии (get_total_income).

    Проверить работу примера на реальных данных (создать экземпляры класса Position,
    передать данные, проверить значения атрибутов, вызвать методы экземпляров).
"""


class Worker:

    def __init__(self, name: str, surname: str, position: str, wage: float, bonus: float):
        self.name = name
        self.surname = surname
        self.position = position
        self._income = {
            'wage': wage,
            'bonus': bonus
        }


class Position(Worker):

    def get_full_name(self) -> str:
        return f'{self.surname} {self.name}'

    def get_total_income(self) -> float:
        return self._income['wage'] + self._income['bonus']
        

if __name__ == '__main__':
    worker_1 = Position(name='Иван', surname='Иванов', position='Аналитик', wage=120000, bonus=30000)
    worker_2 = Position(name="Henry", surname="Ford", position="Driver", wage=80000, bonus=20000)

    print(f'{worker_1.get_full_name()} -> {worker_1.get_total_income()}')
    print(f'{worker_2.get_full_name()} -> {worker_2.get_total_income()}')
