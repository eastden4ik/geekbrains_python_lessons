
"""
    Реализовать скрипт, в котором должна быть предусмотрена функция расчета заработной платы сотрудника.
    В расчете необходимо использовать формулу: (выработка в часах*ставка в час) + премия.
    Для выполнения расчета для конкретных значений необходимо запускать скрипт с параметрами.
"""
from sys import argv


def get_user_salary_info(employee_name: str, work_in_hour: int, rate_per_hours: int, benefit_emp: int) -> str:
    result_salary = int(work_in_hour) * int(rate_per_hours) + int(benefit_emp)
    return f'For employee {employee_name} salary is {result_salary}'


if __name__ == '__main__':
    script_name, employee, work_in_hours, rate_per_hour, benefit = argv
    print(get_user_salary_info(
        employee_name=employee,
        work_in_hour=work_in_hours,
        rate_per_hours=rate_per_hour,
        benefit_emp=benefit))

# To run script: python task1.py Employee 160 720 5000
