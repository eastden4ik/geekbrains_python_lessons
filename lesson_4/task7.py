"""
     Реализовать генератор с помощью функции с ключевым словом yield, создающим очередное значение.
     При вызове функции должен создаваться объект-генератор.
     Функция должна вызываться следующим образом: for el in fact(n).
     Функция отвечает за получение факториала числа,
     а в цикле необходимо выводить только первые n чисел, начиная с 1! и до n!.
     Подсказка: факториал числа n — произведение чисел от 1 до n. Например, факториал четырёх 4! = 1 * 2 * 3 * 4 = 24.
"""
from itertools import count
from math import factorial


def fact(number: int):
    for i in count(1):
        if i <= number:
            yield factorial(i)
        else:
            break


def main():
    num = 6  # Number from which u want to learn factorial
    factorials = []
    for i, el in enumerate(fact(num)):
        if i > 15:
            break
        else:
            print(f'Factorial: {el}')
            factorials.append(el)
            i += 1
    print(f'Factorials: {factorials}')


if __name__ == '__main__':
    main()
