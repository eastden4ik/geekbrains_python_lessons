
"""
     Реализовать два небольших скрипта:
        а) итератор, генерирующий целые числа, начиная с указанного,
        б) итератор, повторяющий элементы некоторого списка, определенного заранее.

    Подсказка: использовать функцию count() и cycle() модуля itertools. Обратите внимание,
    что создаваемый цикл не должен быть бесконечным. Необходимо предусмотреть условие его завершения.

    Например, в первом задании выводим целые числа, начиная с 3,
    а при достижении числа 10 завершаем цикл. Во втором также необходимо предусмотреть условие,
    при котором повторение элементов списка будет прекращено.
"""

from itertools import count
from itertools import cycle


def count_func(start_number, stop_number) -> list:
    result_list = []
    for el in count(start_number):
        if el > stop_number:
            break
        else:
            result_list.append(el)
    return result_list


def cycle_func(my_list, iteration) -> list:
    result_list = []
    i = 0
    iterator = cycle(my_list)
    while i < iteration:
        result_list.append(next(iterator))
        i += 1
    return result_list


def main():
    count_list = count_func(
        start_number=int(input("Type start number: ")),
        stop_number=int(input("Type stop number: ")))
    cycle_list = cycle_func(
        my_list=[3, 4, 9],
        iteration=int(input("Type iteration: ")))

    print(f'count_list: {count_list}')
    print(f'cycle_list: {cycle_list}')


if __name__ == '__main__':
    main()
