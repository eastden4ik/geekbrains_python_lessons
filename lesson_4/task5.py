
"""
     Реализовать формирование списка, используя функцию range() и возможности генератора.
     В список должны войти четные числа от 100 до 1000 (включая границы).
     Необходимо получить результат вычисления произведения всех элементов списка.

    Подсказка: использовать функцию reduce().
"""
from functools import reduce


def multiply_elements(prev_element, element) -> int:
    return prev_element * element


def main():
    example_list = [element for element in range(100, 1001) if element % 2 == 0]
    print(f'Example list: {example_list}')
    print(f'Result of multiplying elements in example list: {reduce(multiply_elements, example_list)}')


if __name__ == '__main__':
    main()
