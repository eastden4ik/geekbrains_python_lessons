

def main():
    """
        Пользователь вводит целое положительное число. Найдите самую большую цифру в числе.
        Для решения используйте цикл while и арифметические операции.
    """
    num = abs(int(input('Введите целое положительное число: ')))
    max_num = num % 10
    while num >= 1:
        num = num // 10
        if num % 10 > max_num:
            max_num = num % 10
        if num > 9:
            continue
        else:
            print("Максимальное цифра в числе ", max_num)
            break


if __name__ == '__main__':
    main()
