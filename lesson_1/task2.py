

def main():
    """
        Пользователь вводит время в секундах.
        Переведите время в часы, минуты и секунды и выведите в формате чч:мм:сс.
        Используйте форматирование строк.
    """
    seconds_typed = int(input('введите количество секунд: '))
    minutes, seconds = divmod(seconds_typed, 60)
    hours, minutes = divmod(minutes, 60)
    print(f'{hours:02d}:{minutes:02d}:{seconds:02d}')


if __name__ == '__main__':
    main()
