

def main():
    """
        Узнайте у пользователя число n. Найдите сумму чисел n + nn + nnn.
        Например, пользователь ввёл число 3. Считаем 3 + 33 + 333 = 369.
    """
    num_1 = int(input('введите число: '))
    num_2 = str(num_1) + str(num_1)
    num_3 = str(num_1) + str(num_1) + str(num_1)
    print(f'{num_1} + {num_2} + {num_3} = {int(num_1) + int(num_2) + int(num_3)}')


if __name__ == '__main__':
    main()
