
"""
    Пользователь вводит месяц в виде целого числа от 1 до 12.
    Сообщить к какому времени года относится месяц (зима, весна, лето, осень).
    Напишите решения через list и через dict.
"""


def main():
    seasons_dict = {'зима': [1, 2, 12], 'весна': [3, 4, 5], 'лето': [6, 7, 8], 'осень': [9, 10, 11]}
    seasons_list = ['зима', 'зима', 'весна', 'весна', 'весна', 'лето', 'лето', 'лето', 'осень', 'осень', 'осень', 'зима']

    user_input = int(input('Type number of month: '))
    print(f'From list month {user_input} is in {seasons_list[user_input - 1]}')
    for season, months in seasons_dict.items():
        if user_input - 1 in months:
            print(f'From dictionary month {user_input} is in {season}')
            break


if __name__ == '__main__':
    main()
