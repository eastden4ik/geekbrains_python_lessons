
"""
    Реализовать структуру «Рейтинг», представляющую собой не возрастающий набор натуральных чисел.
    У пользователя необходимо запрашивать новый элемент рейтинга.
    Если в рейтинге существуют элементы с одинаковыми значениями,
    то новый элемент с тем же значением должен разместиться после них.
"""


def main():

    example_list = [7, 5, 3, 3, 2]
    quit_app = False

    while not quit_app:
        user_input = input('Type number or "quit" to close app: ')
        if user_input == 'quit':
            quit_app = True
        else:
            example_list.append(int(user_input))
            example_list.sort(reverse=True)
            print(f'Пользователь ввел число {user_input}. Результат: {example_list}.')


if __name__ == '__main__':
    main()
