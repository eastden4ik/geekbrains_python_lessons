
"""
    Для списка реализовать обмен значений соседних элементов, т.е.
    Значениями обмениваются элементы с индексами 0 и 1, 2 и 3 и т.д.
    При нечетном количестве элементов последний сохранить на своем месте.
    Для заполнения списка элементов необходимо использовать функцию input().
"""


def main():
    # elements = [1, 2, 3, 4, 5, 6, 7, 8, 9]
    elements = input('Type numbers with comma. Ex: 1, 2, 3, 4. : ').split(',')

    print(f'Elements before: {elements}')
    if len(elements) % 2 == 0:
        i = 0
        while i < len(elements):
            elements[i], elements[i + 1] = elements[i + 1], elements[i]
            i += 2
    else:
        i = 0
        while i < len(elements) - 1:
            elements[i], elements[i + 1] = elements[i + 1], elements[i]
            i += 2
    print(f'Elements after: {elements}')


if __name__ == '__main__':
    main()
