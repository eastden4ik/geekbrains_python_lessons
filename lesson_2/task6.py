
"""
    Реализовать структуру данных «Товары». Она должна представлять собой список кортежей.
    Каждый кортеж хранит информацию об отдельном товаре. В кортеже должно быть два элемента —
    номер товара и словарь с параметрами (характеристиками товара: название, цена, количество, единица измерения).
    Структуру нужно сформировать программно, т.е. запрашивать все данные у пользователя.

    Пример готовой структуры:
    [
        (1, {“название”: “компьютер”, “цена”: 20000, “количество”: 5, “eд”: “шт.”}),
        (2, {“название”: “принтер”, “цена”: 6000, “количество”: 2, “eд”: “шт.”}),
        (3, {“название”: “сканер”, “цена”: 2000, “количество”: 7, “eд”: “шт.”})
    ]
    Необходимо собрать аналитику о товарах. Реализовать словарь, в котором каждый ключ — характеристика товара,
    например название, а значение — список значений-характеристик, например список названий товаров.

    Пример:

    {
    “название”: [“компьютер”, “принтер”, “сканер”],
    “цена”: [20000, 6000, 2000],
    “количество”: [5, 2, 7],
    “ед”: [“шт.”]
    }
"""


def main():

    example_structure = []
    quit_app = False

    i = 1
    while not quit_app:
        user_input = input('Type "quit" to close app, "add" to add product, '
                           '"watch" to watch example_structure and "analytics" to get analytics: ')
        if user_input == 'quit':
            quit_app = True
        elif user_input == 'add':
            product_dict = {}
            product_name = input('Type name of the product: ')
            product_price = int(input('Type price of the product: '))
            product_amount = int(input('Type amount of the product: '))
            product_unit = input('Type unit of the product: ')
            product_dict["название"] = product_name
            product_dict["цена"] = product_price
            product_dict["количество"] = product_amount
            product_dict["ед"] = product_unit
            example_structure.append(tuple([i, product_dict]))
            i += 1
        elif user_input == 'analytics':
            analytics = {}
            for good in example_structure:
                for product_key, product_value in good[1].items():
                    if product_key in analytics:
                        analytics[product_key].append(product_value)
                    else:
                        analytics[product_key] = [product_value]
            print(f'Analytics is {analytics}')
        elif user_input == 'watch':
            print(f'{example_structure}')
        else:
            print('u typed something wrong...')


if __name__ == '__main__':
    main()
