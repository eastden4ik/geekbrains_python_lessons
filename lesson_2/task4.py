
"""
    Пользователь вводит строку из нескольких слов, разделённых пробелами.
    Вывести каждое слово с новой строки. Строки необходимо пронумеровать.
    Если в слово длинное, выводить только первые 10 букв в слове.
"""


def main():

    string = input('Type words with spaces between them: ')

    for i, word in enumerate(string.split()):
        if len(word) > 10:
            print(f'{i} - {word[:10]}')
        else:
            print(f'{i} - {word}')


if __name__ == '__main__':
    main()
