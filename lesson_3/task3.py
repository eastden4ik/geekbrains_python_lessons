
"""
    Реализовать функцию my_func(), которая принимает три позиционных аргумента, и
    возвращает сумму наибольших двух аргументов.
"""


def my_func(*args) -> int:
    ex_list = list(args)
    max_1 = max(ex_list)
    ex_list.remove(max_1)
    max_2 = max(ex_list)
    print(f'max_1 = {max_1}, max_2 = {max_2}')
    return max_1 + max_2


def main():
    print(f'132 + 215 = {my_func(132, 215, 21)}')


if __name__ == '__main__':
    main()
