
"""
    Реализовать функцию, принимающую несколько параметров, описывающих данные пользователя:
    имя, фамилия, год рождения, город проживания, email, телефон.
    Функция должна принимать параметры как именованные аргументы.
    Реализовать вывод данных о пользователе одной строкой.
"""


def get_info_about(
        first_name: str,
        last_name: str,
        year: int,
        city: str,
        email: str,
        phone: str) -> str:
    return f'first_name: {first_name},\n' \
           f'last_name: {last_name},\n' \
           f'year: {year},\n' \
           f'city: {city},\n' \
           f'email: {email},\n' \
           f'phone: {phone}'


def main():
    info = get_info_about(
        first_name='Иван',
        last_name='Иванов',
        year=1995,
        city='Владивосток',
        email='example@mail.ru',
        phone='8-800-555-35-35')
    print(info)


if __name__ == '__main__':
    main()
