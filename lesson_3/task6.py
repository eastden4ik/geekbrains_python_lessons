
"""
    Реализовать функцию int_func(), принимающую слово из маленьких латинских букв и возвращающую его же,
    но с прописной первой буквой. Например, print(int_func(‘text’)) -> Text.

    Продолжить работу над заданием. В программу должна попадать строка из слов, разделенных пробелом.
    Каждое слово состоит из латинских букв в нижнем регистре. Сделать вывод исходной строки,
    но каждое слово должно начинаться с заглавной буквы. Необходимо использовать написанную ранее функцию int_func().
"""


def int_func(word: str) -> str:
    return word.title()


def main():
    words = ['hello', 'world', 'test']
    for word in words:
        print(f'Initial word is {word} and modified is {int_func(word=word)}.')

    print('')
    example_sentence = 'test sentence for testing six task in third lesson in geek brains'
    print(f'Sentence before int_func: {example_sentence}')

    example_sentence = ' '.join([int_func(word=word) for word in example_sentence.split(' ')])
    print(f'Sentence after int_func: {example_sentence}')


if __name__ == '__main__':
    main()
