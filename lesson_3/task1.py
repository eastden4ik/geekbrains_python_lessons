
"""
    Реализовать функцию, принимающую два числа (позиционные аргументы) и выполняющую их деление.
    Числа запрашивать у пользователя, предусмотреть обработку ситуации деления на ноль.
"""


def div_two_nums(*args) -> float:
    try:
        result = args[0] / args[1]
        return result
    except ZeroDivisionError as e:
        print(f'You typed zero for second argument. Its an error. Exception: {e}.')
        raise SystemExit(1)


def main():
    num_1 = int(input('Type first number: '))
    num_2 = int(input('Type second number: '))
    result = div_two_nums(num_1, num_2)
    print(f'{num_1} / {num_2} = {result}')


if __name__ == '__main__':
    main()
