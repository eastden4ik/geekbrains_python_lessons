
"""
    Программа принимает действительное положительное число x и целое отрицательное число y.
    Необходимо выполнить возведение числа x в степень y. Задание необходимо реализовать в виде функции my_func(x, y).
    При решении задания необходимо обойтись без встроенной функции возведения числа в степень.
"""


def my_func_1(x: float, y: int) -> float:
    return x ** y


def my_func_2(x: float, y: int) -> float:
    a = 1
    if y < 0:
        x = 1 / x
        y = abs(y)
    if not (y % 2 == 0):
        a = x
        y -= 1
    while y != 1:
        x *= x
        y /= 2
    x *= a
    return x


def my_func_3(x: float, y: int) -> float:
    if y < 0:
        x = 1 / x
        y = abs(y)
    if y == 0:
        return 1
    else:
        return x * my_func_3(x, y - 1)


def main():
    print(f' 1/8 в степени -3 (using **) = {my_func_1(x=1/8, y=-3)}')
    print(f' 1/8 в степени -3 (using while) = {my_func_2(x=1/8, y=-3)}')
    print(f' 1/8 в степени -3 (using recursion) = {my_func_3(x=1/8, y=-3)}')


if __name__ == '__main__':
    main()
