
"""
    Программа запрашивает у пользователя строку чисел, разделенных пробелом.
    При нажатии Enter должна выводиться сумма чисел.
    Пользователь может продолжить ввод чисел, разделенных пробелом и снова нажать Enter.
    Сумма вновь введенных чисел будет добавляться к уже подсчитанной сумме.
    Но если вместо числа вводится специальный символ, выполнение программы завершается.
    Если специальный символ введен после нескольких чисел,
    то вначале нужно добавить сумму этих чисел к полученной ранее сумме и после этого завершить программу.
"""


def sum_of_user_sequence(tokens: list) -> float:
    sum_of_tokens = 0
    for token in tokens:
        sum_of_tokens += float(token)
    return sum_of_tokens


def main():
    quit_app = False
    result = 0

    while not quit_app:
        user_input = input('Type numbers with space between them or type "quit" to quit app: ')
        tokens = user_input.split(' ')
        if 'quit' in tokens:
            tokens.remove('quit')
            result += sum_of_user_sequence(tokens)
            quit_app = True
        else:
            result += sum_of_user_sequence(tokens)
    print(f'Result sum of entered numbers is {result}')


if __name__ == '__main__':
    main()
