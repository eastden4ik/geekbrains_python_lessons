
"""
    Создать текстовый файл (не программно), сохранить в нем несколько строк,
    выполнить подсчет количества строк, количества слов в каждой строке.
"""


if __name__ == '__main__':

    file_name = 'files/task2.txt'
    words_per_line = {}
    with open(file_name, mode='r', encoding='utf-8') as file:
        lines = file.readlines()
    for i, line in enumerate(lines):
        words_per_line[i + 1] = len(line.split(' '))

    print(f'В файле {len(words_per_line)} строк')
    for key, val in words_per_line.items():
        print(f'В {key}-й строке {val} слов')

