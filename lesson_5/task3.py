
"""
    Создать текстовый файл (не программно),
    построчно записать фамилии сотрудников и величину их окладов (не менее 10 строк).
    Определить, кто из сотрудников имеет оклад менее 20 тыс.,
    вывести фамилии этих сотрудников.
    Выполнить подсчет средней величины дохода сотрудников.

    Пример файла:
    Иванов 23543.12
    Петров 13749.32
"""


if __name__ == '__main__':

    file_name = 'files/task3.txt'
    words_per_line = {}
    with open(file_name, mode='r', encoding='utf-8') as file:
        lines = file.readlines()

    sum_of_incomes = 0
    for i, line in enumerate(lines):
        last_name, salary = line.split(' ')
        if float(salary) <= 20000:
            print(f'Last_name {last_name} - {salary}')
        sum_of_incomes += float(salary)

    print(f'Average salary of the employees: {sum_of_incomes / len(lines)}')
