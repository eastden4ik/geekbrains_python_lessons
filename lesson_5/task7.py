
"""
    Создать вручную и заполнить несколькими строками текстовый файл,
    в котором каждая строка должна содержать данные о фирме:
    название, форма собственности, выручка, издержки.

    Пример строки файла: firm_1 ООО 10000 5000.

    Необходимо построчно прочитать файл, вычислить прибыль каждой компании, а также среднюю прибыль.
    Если фирма получила убытки, в расчет средней прибыли ее не включать.
    Далее реализовать список. Он должен содержать словарь с фирмами и их прибылями,
    а также словарь со средней прибылью. Если фирма получила убытки,
    также добавить ее в словарь (со значением убытков).
    Пример списка: [{“firm_1”: 5000, “firm_2”: 3000, “firm_3”: 1000}, {“average_profit”: 2000}].

    Итоговый список сохранить в виде json-объекта в соответствующий файл.
    Пример json-объекта:

    [{"firm_1": 5000, "firm_2": 3000, "firm_3": 1000}, {"average_profit": 2000}]
"""
import json


if __name__ == '__main__':

    file_name_txt = 'files/task7.txt'
    file_name_json = 'files/task7.json'

    result = []
    profit = {}
    loss = {}
    average = []

    with open(file_name_txt, mode='r', encoding='utf-8') as file:
        lines = file.readlines()

        for line in lines:
            elements = line.split()

            temp = float(elements[-2]) - float(elements[-1])
            if temp >= 0:
                profit[elements[0]] = temp
                average.append(temp)
            else:
                loss[elements[0]] = temp

    average_amount = sum(average) / len(average)

    result = [profit, {"average_profit": average_amount}, loss]
    with open(file_name_json, mode='w', encoding='utf-8') as file:
        json.dump(result, file)
