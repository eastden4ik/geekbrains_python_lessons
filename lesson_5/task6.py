
"""
    Необходимо создать (не программно) текстовый файл,
    где каждая строка описывает учебный предмет и наличие лекционных,
    практических и лабораторных занятий по этому предмету и их количество.
    Важно, чтобы для каждого предмета не обязательно были все типы занятий.
    Сформировать словарь, содержащий название предмета и общее количество занятий по нему.
    Вывести словарь на экран.

    Примеры строк файла:
    Информатика: 100(л) 50(пр) 20(лаб).
    Физика: 30(л) - 10(лаб)
    Физкультура: - 30(пр) -

    Пример словаря: {“Информатика”: 170, “Физика”: 40, “Физкультура”: 30}
"""
import re


if __name__ == '__main__':

    file_name = 'files/task6.txt'

    with open(file_name, mode='r', encoding='utf-8') as file:
        lines = file.readlines()

        subject_dict = {}
        for line in lines:
            subject, amount_of_subjects = line.split(': ')
            line_of_hours = re.sub(r'\D', ' ', amount_of_subjects)
            ttl_hours = 0
            for hour in line_of_hours.split():
                ttl_hours += float(hour)
            subject_dict[subject] = ttl_hours

        print('Final dictionary: ')
        for key, val in subject_dict.items():
            print(f'{key} - {val}')
