
"""
    Создать (программно) текстовый файл, записать в него программно набор чисел, разделенных пробелами.
    Программа должна подсчитывать сумму чисел в файле и выводить ее на экран.
"""
from random import randint


if __name__ == '__main__':

    file_name = 'files/task5.txt'

    numbers = ' '.join([str(randint(1, 10)) for _ in range(100)])
    with open(file_name, mode='w', encoding='utf-8') as file:
        file.write(numbers)

    with open(file_name, mode='r', encoding='utf-8') as file:
        line = file.read()

        sum_in_file = 0
        for num in line.split(' '):
            sum_in_file += int(num)

    print(f'Sum of numbers from file [{file_name}] is {sum_in_file}')
