
"""
    Создать (не программно) текстовый файл со следующим содержимым:
        One — 1
        Two — 2
        Three — 3
        Four — 4

    Необходимо написать программу, открывающую файл на чтение и считывающую построчно данные.
    При этом английские числительные должны заменяться на русские.
    Новый блок строк должен записываться в новый текстовый файл.
"""
from translate import Translator


if __name__ == '__main__':

    file_name_en = 'files/task4_en.txt'
    file_name_ru_translator = 'files/task4_ru_translator.txt'
    file_name_ru_dict = 'files/task4_ru_dict.txt'

    with open(file_name_en, mode='r', encoding='utf-8') as file:
        lines = file.readlines()
        print(f'All lines were read from file [{file_name_en}]')

    # С использванием модуля Translator
    translator = Translator(to_lang='ru')
    ru_lines = []
    for i, line in enumerate(lines):
        letters_num_en, digits_num = line.split(' — ')
        letters_num_ru = translator.translate(letters_num_en)
        ru_lines.append(letters_num_ru + ' — ' + digits_num)

    with open(file_name_ru_translator, mode='w', encoding='utf-8') as file:
        file.writelines(ru_lines)
        print(f'All translated lines were written to file [{file_name_ru_translator}]')

    # С использованием словаря
    en_ru_dict = {
        'One': 'Один',
        'Two': 'Два',
        'Three': 'Три',
        'Four': 'Четыре',
        'Five': 'Пять',
        'Six': 'Шесть',
        'Seven': 'Семь',
        'Eight': 'Восемь',
        'Nine': 'Девять',
        'Ten': 'Десять'
    }

    ru_lines = []
    for i, line in enumerate(lines):
        letters_num_en, digits_num = line.split(' — ')
        letters_num_ru = en_ru_dict[letters_num_en]
        ru_lines.append(letters_num_ru + ' — ' + digits_num)

    with open(file_name_ru_dict, mode='w', encoding='utf-8') as file:
        lines = file.writelines(ru_lines)
        print(f'All translated lines were written to file [{file_name_ru_dict}]')


