
"""
    Создать программно файл в текстовом формате, записать в него построчно данные,
    вводимые пользователем. Об окончании ввода данных свидетельствует пустая строка.
"""


if __name__ == '__main__':

    file_name = 'files/task1.txt'
    with open(file_name, mode='a', encoding='utf-8') as file:

        while True:
            user_input = input('Type line to the file or save empty line to stop: ')
            if not user_input:
                file.close()
                break
            file.write(user_input + '\n')

