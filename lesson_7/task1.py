
"""
    Реализовать класс Matrix (матрица). Обеспечить перегрузку конструктора класса (метод init()),
    который должен принимать данные (список списков) для формирования матрицы.

    Подсказка: матрица — система некоторых математических величин, расположенных в виде прямоугольной схемы.
    Примеры матриц: см. в методичке.

    Следующий шаг — реализовать перегрузку метода str() для вывода матрицы в привычном виде.
    Далее реализовать перегрузку метода add() для реализации операции сложения
    двух объектов класса Matrix (двух матриц).
    Результатом сложения должна быть новая матрица.
    Подсказка: сложение элементов матриц выполнять поэлементно — первый элемент первой строки
    первой матрицы складываем с первым элементом первой строки второй матрицы и т.д.
"""
from typing import List


class Matrix:

    def __init__(self, table: List[list]):
        self._matrix = table

    def __str__(self):
        return "\n".join([" ".join(map(str, row)) for row in self._matrix])

    def __add__(self, other):
        result = []
        for i in zip(self._matrix, other._matrix):
            result.append([sum([j, k]) for j, k in zip(*i)])
        return Matrix(result)


if __name__ == '__main__':
    matrix_1 = Matrix([[31, 22], [37, 43], [51, 86]])
    matrix_2 = Matrix([[3, 5], [2, 4], [-1, 64]])
    matrix_3 = Matrix([[34, 27], [39, 47], [50, 150]])

    print(f'Expected values:\n{matrix_1}\n+\n{matrix_2}\n=\n{matrix_3}')
    print(f'Actual values:\n{matrix_1}\n+\n{matrix_2}\n=\n{matrix_1 + matrix_2}')
