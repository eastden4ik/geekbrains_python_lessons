"""
    Начните работу над проектом «Склад оргтехники». Создайте класс, описывающий склад. А также класс «Оргтехника»,
    который будет базовым для классов-наследников. Эти классы — конкретные типы оргтехники (принтер, сканер, ксерокс).
    В базовом классе определить параметры, общие для приведенных типов. В классах-наследниках реализовать параметры,
    уникальные для каждого типа оргтехники.
"""


class Storage:
    pass


class OfficeEquipment:

    def __init__(self, vendor: str, category: str, model: str, color: str, price: float):
        self.vendor = vendor
        self.category = category
        self.model = model
        self.color = color
        self.price = price

    def __str__(self):
        return {
            'vendor': self.vendor,
            'category': self.category,
            'model': self.model,
            'color': self.color,
            'price': self.price,
        }


class Printer(OfficeEquipment):
    def __init__(self, is_wireless: bool, *args):
        super().__init__("Printer", *args)
        self.is_wireless = is_wireless


class Scanner(OfficeEquipment):
    def __init__(self, *args):
        super().__init__("Scanner", *args)


class Xerox(OfficeEquipment):
    def __init__(self, is_colorful: bool, *args):
        super().__init__("Xerox", *args)
        self.is_colorful = is_colorful
