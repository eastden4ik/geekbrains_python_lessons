"""
    Создайте собственный класс-исключение, обрабатывающий ситуацию деления на нуль.
    Проверьте его работу на данных, вводимых пользователем.
    При вводе пользователем нуля в качестве делителя программа должна корректно
    обработать эту ситуацию и не завершиться с ошибкой.
"""


class ZeroDivisionMyError(Exception):
    text = "!!! Делить на ноль нельзя !!!"

    def __str__(self):
        return self.text


if __name__ == '__main__':
    try:
        dividend = int(input("Введите делимое: "))
        divisor = int(input("Введите делитель: "))
        if divisor == 0:
            raise ZeroDivisionMyError
        result = int(dividend / divisor)
        print(f"Результат деления {dividend}/{divisor}={result}")
    except ZeroDivisionMyError as e:
        print(e)
