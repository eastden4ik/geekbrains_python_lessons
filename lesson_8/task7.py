"""
    Реализовать проект «Операции с комплексными числами».
    Создайте класс «Комплексное число», реализуйте перегрузку методов сложения и умножения комплексных чисел.
    Проверьте работу проекта, создав экземпляры класса (комплексные числа) и
    выполнив сложение и умножение созданных экземпляров.
    Проверьте корректность полученного результата.
"""


class Complex:
    def __init__(self, real, img=0.0):
        self.real = real
        self.img = img

    def __add__(self, other):
        return Complex(self.real + other.real, self.img + other.img)

    def __mul__(self, other):
        return Complex((self.real * other.real) - (self.img * other.img),
                             (self.img * other.real) + (self.real * other.img))

    def __str__(self):
        return f"{self.real + self.img}"


if __name__ == '__main__':
    complex_number01 = Complex(15, 9j)
    complex_number02 = Complex(22, 2j)
    print(complex_number01 + complex_number02)
    print(complex_number01 * complex_number02)
